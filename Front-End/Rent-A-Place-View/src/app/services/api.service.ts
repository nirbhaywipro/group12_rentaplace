import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SessionService } from './storage/session.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient,
    private session: SessionService) { }
  getAuthHeader() {
    const token = this.session.getToken();
    return new HttpHeaders().set("Authorization", "Bearer " + token);
  }

  get(url: string) {
    return this.http.get(environment.apiBaseUrl + url, { headers: this.getAuthHeader() });
  }
  post(url: string, payload: any) {
    return this.http.post(environment.apiBaseUrl + url, payload, { headers: this.getAuthHeader() });
  }
  postWithoutAuth(url: string, payload: any) {
    return this.http.post(environment.apiBaseUrl + url, payload);
  }
  patch(url: string, payload: any) {
    return this.http.patch(environment.apiBaseUrl + url, payload, { headers: this.getAuthHeader() });
  }
  put(url: string, payload: any) {
    return this.http.put(environment.apiBaseUrl + url, payload, { headers: this.getAuthHeader() });
  }
  delete(url: string, payload?: any) {
    if (!payload) {
      return this.http.delete(environment.apiBaseUrl + url, { headers: this.getAuthHeader() });
    }
    return this.http.delete(environment.apiBaseUrl + url, {
      body: payload
    });
  }

}
