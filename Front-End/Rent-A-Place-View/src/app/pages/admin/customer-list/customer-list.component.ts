import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { ApiService } from 'src/app/services/api.service';
import { CustomerDataType, CustomerService } from 'src/app/services/customer/customer.service';
import { SessionService } from 'src/app/services/storage/session.service';

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {
  usersList: CustomerDataType[] = [];
  bookings: any[] = [];
  displayedColumns: string[] = ['propertyName', 'propertyAddress', 'propertyType', 'bookedByName', 'bookedByEmail', 'checkInDate', 'checkOutDate', 'totalAmount', 'cnfBtn'];
  allRecordsCount = 0;
  pageSize = 10;
  isAdmin = false;
  userId = 0;
  pageEvent: any;

  constructor(
    private customer: CustomerService,
    private api: ApiService,
    private session: SessionService
  ) { }

  ngOnInit(): void {
    this.isAdmin = this.session.isAdmin();
    this.userId = this.session.getUsers()?.user?.userId;
    this.getBookings();
  }

  getBookings() {

    const url = this.isAdmin ? '/api/booking/all' : `/api/booking/all/${this.userId}`;
    this.api.get(url).subscribe((res: any) => {
      this.bookings = res?.data?.map((item: any) => ({
        propertyName: item?.property?.name,
        propertyAddress: item?.property?.address,
        propertyType: item?.property?.type,
        bookedByName: item?.bookedBy?.name,
        bookedByEmail: item?.bookedBy?.email,
        totalAmount: item?.totalPayment,
        checkInDate: item?.checkInDate,
        checkOutDate: item?.checkOutDate,
        bookingId: item?.id,
        isConfirmed: item?.isConfirmed
      }));
    });
  }

  confirmBooking(bookingId: number) {
    console.log(bookingId);
    this.api.get(`/api/booking/confirm/${bookingId}`).subscribe(res => this.getBookings());
  }



}
