import { Component, Inject, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FeedbackComponent } from 'src/app/pages/admin/feedback/feedback.component';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api.service';
@Component({
  selector: 'app-message-reply',
  templateUrl: './message-reply.component.html',
  styleUrls: ['./message-reply.component.scss']
})
export class MessageReplyComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<FeedbackComponent>,
    private alert: AlertService,
    private api: ApiService,
    @Inject(MAT_DIALOG_DATA) public data: { senderId: number, receiverId: number }) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  formSubmit(formRef: NgForm) {
    if (formRef.form.status === 'INVALID') {
      this.alert.show({
        title: "Required fields",
        message: "Please fill all required fields",
        icon: 'warning'
      });
      return;
    }


    this.api.post(`/api/messaging/send`, {
      sender: this.data.receiverId,
      receiver: this.data.senderId,
      message: formRef.form.value?.replyMessage
    }).subscribe((res) => {
      this.alert.show({
        title: "Message sent",
        message: "Reply sent",
        icon: 'success'
      });
      this.onNoClick()
    })

  }

}
