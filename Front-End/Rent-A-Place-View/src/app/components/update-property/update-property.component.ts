import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdminDashboardComponent } from 'src/app/pages/admin/admin-dashboard/admin-dashboard.component';
import { AlertService } from 'src/app/services/alert/alert.service';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-update-property',
  templateUrl: './update-property.component.html',
  styleUrls: ['./update-property.component.scss']
})
export class UpdatePropertyComponent implements OnInit {
  propertyType: string = '';
  propertyName: string = '';
  propertyAddress: string = '';
  propertyPrice: string = '';
  constructor(
    public dialogRef: MatDialogRef<AdminDashboardComponent>,
    private alert: AlertService,
    private api: ApiService,
    @Inject(MAT_DIALOG_DATA) public data: { property: any }
  ) { }

  ngOnInit(): void {
    this.propertyName = this.data.property?.name;
    this.propertyType = this.data.property?.type;
    this.propertyAddress = this.data.property?.address;
    this.propertyPrice = this.data.property?.price;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
  updateProperty() {
    this.api.put(`/api/properties/update`, {
      name: this.propertyName,
      address: this.propertyAddress,
      propertyId: this.data.property?.id,
      totalRooms: 0,
      type: this.propertyType,
      price: this.propertyPrice,
      available: true,
      active: true
    }).subscribe(res => {
      this.alert.single("Successfully updated", 'success');
      this.dialogRef.close();
    });
  }
}
