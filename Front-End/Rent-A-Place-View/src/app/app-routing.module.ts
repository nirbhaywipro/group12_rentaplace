import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminGuard } from './guards/admin.guard';
import { AuthGuard } from './guards/auth.guard';
import { AddImagesComponent } from './pages/add-images/add-images.component';
import { AdminDashboardComponent } from './pages/admin/admin-dashboard/admin-dashboard.component';
import { AdminProductComponent } from './pages/admin/admin-product/admin-product.component';
import { CustomerListComponent } from './pages/admin/customer-list/customer-list.component';
import { FeedbackComponent } from './pages/admin/feedback/feedback.component';
import { LayoutComponent } from './pages/admin/layout/layout.component';
import { BookingComponent } from './pages/booking/booking.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HomeComponent } from './pages/home/home.component';
import { SignInComponent } from './pages/sign-in/sign-in.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { UserFeedbackComponent } from './pages/user-feedback/user-feedback.component';

const routes: Routes = [
  {
    path: 'signup',
    component: SignUpComponent,
  },
  {
    path: 'sign',
    component: SignInComponent,
  },
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '', // child route path
        component: DashboardComponent, // child route component that the router renders
      },
      {
        path: 'Booking',
        component: BookingComponent,
      },

      {
        path: 'my-order',
        component: CustomerListComponent, // another child route component that the router renders
      },
      {
        path: 'feedback',
        component: FeedbackComponent, // another child route component that the router renders
      },
    ],
  },
  {
    path: 'admin',
    component: LayoutComponent,
    canActivate: [AdminGuard],
    canActivateChild: [AdminGuard],
    children: [
      {
        path: '',
        component: AdminDashboardComponent,
      },
      {
        path: 'product',
        component: AdminProductComponent,
      },
      {
        path: 'customer-list',
        component: CustomerListComponent,
      },
      {
        path: 'add-image',
        component: AddImagesComponent,
      },


      {
        path: 'Booking',
        component: BookingComponent,
      },
      {
        path: 'feedback',
        component: FeedbackComponent,
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
